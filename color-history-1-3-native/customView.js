/*
  In dieser Darstellungsform wird der Zustand jeder der 24 Zellen des AUtomaten als eine Stelle eines 24-bit RGB Farbwerts
  interpretiert. Dieser wird als Farbstreifen ausgegeben. Die Höhe der Zellen in der zuschaltbaren Standarddarstellung orientiert
  sich an der einstellbaren höhe der Farbstreifen.
 */ 

function View(viewmodel) 
{
  let self = this;
  this._vm = viewmodel;
  this.context = p5CanvasElement.getContext("2d");

  this.initialise = function(){
    background(255);
    noStroke();
  };

  this.render = function(automaton)
  { 
    if(this.isFullscreen)
    {
        scale(scale, scale);
    }
    
    //neu: mit Scrollen
    this.context.clearRect(0,0,p5CanvasElement.width, p5CanvasElement.height);
    for (let row = this._vm._colorHistory.length- 1; row>=0; row--)
    {  
      this.context.fillStyle = 'rgb(' + this._vm._colorHistory[row][0]+','+this._vm._colorHistory[row][1]+','+this._vm._colorHistory[row][2]+')';
      if(this._vm.isCyclic)
      {
         let radius = this._vm.getGenerationRadius(row);
         //this.context.save();
         this.context.save();
         this.context.translate(p5CanvasElement.width/2, p5CanvasElement.height/2);
         this.context.ellipse (0,0,radius, radius,0,0,2* Math.PI);
         this.context.fill();
         this.context.restore();
         //this.context.restore();
      }
      else
      {
        this.context.fillRect(0, this._vm.getBarPositionY(row), width, this._vm._getBarWidth());
        }  
    }
    this.setIsRendered(true);
  }
}
