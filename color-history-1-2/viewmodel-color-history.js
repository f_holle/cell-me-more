function ViewModel(automato) 
{
  ////////////////////////////////////////NICHT ÄNDERN/////////////////////////////////////////

  let self = this; ///kann benutzt werden, wenn die Bezeichnung "this" missverständlich ist
  this._automaton = automato;
  this._isLooping = false;
  this.observers = [];

  this._cellWidth = 5;///Startwert

  this._bufferWidth = width;
  this.segmentWidth = 0;(height-10)/ this.maxDisplayableGenerations;


  //Klassenmethoden
  this.initialise = function()
  {
    this.initialiseDefault();
    this.segmentWidth = (height-10)/ this.maxDisplayableGenerations;
  };

  ////////////////////////////Programmabhängige Methoden für Custom View hier einfügen//////////

  this.calculateR = function(row)
  {
    let rString = "";
    for (let i = 0; i<8; i++)
    {
      rString = rString+ this.displayMatrix[row][i];
    }
    let rValue = parseInt(rString,2);
    return rValue;
  };
  
  this.calculateG = function(row)
  {
    let gString = "";
    for (let i = 8; i<16; i++)
    {
      gString = gString+ this.displayMatrix[row][i];
    }
    let gValue = parseInt(gString,2);
    return gValue;
  };
  
  this.calculateB = function(row)
  {
    let bString = "";
    for (let i = 16; i<this._automaton._cells.length; i++)
    {
      bString = bString+ this.displayMatrix[row][i];
    }
    let bValue = parseInt(bString,2);
    return bValue;
  };

  this.getRadius = function (index)
  {
    const radius = (index + 1) * this.segmentWidth;
    return radius;
  }

}
