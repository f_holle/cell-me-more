function View(viewmodel) 
{
  let self = this;
  this._vm = viewmodel;

  this._xOrigin = this._vm._bufferWidth/2;
  this._yOrigin = height/2;  

  this.initialise = function()
  {
    background(255);
    noFill();
  };

  this.render = function(){
    /////füllen
    {
      for(let i =0; i<this._vm.currentStatesArray; i++)
      {      
        if (this._vm.currentStatesArray[i] ==1)
        {
          stroke(0);
          strokeWeight(this._vm._cellWidth*0.5);
          strokeCap(SQUARE);
          arc(this._xOrigin, this._yOrigin, this._vm.calculateGenerationRadius(this._vm._automaton._generationCount), 
          this._vm.calculateGenerationRadius(this._vm._automaton._generationCount), this._vm.calculateArcStart(i), this._vm.calculateArcStop(i));      
        }
      }
    }  
    this.setIsRendered(true);
  };

  ///////////////////
  this.renderSingleCellSegments = function(index)
  {
    const renderedCell = index;
    for (let i = this._vm._automaton._generationCount; i>=0; i--)
    {
      if (this._vm._automaton._cells[renderedCell]._history[i]==1)
      {
        fill(0);
      }
      else
      {
        fill(255);
      } 
        arc(this._xOrigin, this._yOrigin, this._vm.calculateGenerationRadius(i), 
        this._vm.calculateGenerationRadius(i), this._vm.calculateArcStart(renderedCell), this._vm.calculateArcStop(renderedCell));      
    }
  };
}
