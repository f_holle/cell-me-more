function ViewModel(automato) 
{
  ////////////////////////////////////////NICHT ÄNDERN/////////////////////////////////////////
  let self = this; ///kann benutzt werden, wenn die Bezeichnung "this" missverständlich ist
  this._automaton = automato;
  this._isLooping = false;
  this.observers = [];

  this._cellWidth = 0;
  this._edge = 0;

  this._bufferGap = width*0.05;
  this._bufferWidth = (width/2) - this._bufferGap;
  this._additionalViewPosition = this._bufferWidth + this._bufferGap;

  ////Variablen für die Berechnung der Kreissegmente
  this.renderInformation = [];
  this._segmentHeight = 0;

  //Klassenmethoden
  this.initialise = function()
  {
    this.calculateCellSize();
    angleMode(RADIANS);
    noStroke();
    this._segmentHeight = this._cellWidth;
    this._segmentArc = TWO_PI/this._automaton._cellCount;
  }

  ///gui-Input Callback Methoden
  
  ////// Schaltet die draw-Funtion in Abhängigkeit von IsLooping an oder ab
  this.runPauseButtonAction = function()
  {
    const boolean = !self._isLooping;
    self.setIsLooping(boolean);
    
    if (self._isLooping) 
    {
      loop();
    }
    else
    {
      noLoop();
    }
  };
  
  ///berechnet und rendert eine einzelne Generation
  this.renderOneGenerationButtonAction = function()
  {
    self.setIsLooping(false);
    noLoop();
    self._automaton.generateGeneration();  
  }

  this.changeArrayMode = function ()
  {
    console.log(self._automaton._isCyclic);
    if (self._automaton._isCyclic == false)
    {      
      console.log("hallo");
      self._automaton.setIsCyclic(true);
      self._automaton.closeRingGrid(); 
      console.log("is Cyclic");
    }
    else
    {
      self._automaton.disconnectRingGrid();
      self._automaton.setIsCyclic(false);
      console.log("isNotCyclic");
    }         
  } 

  ///wird von von den Regel-Inputs aufgerufen
  ///als Parameter wird die Change-Funktion des jeweiligen Inputs übergeben, die die
  ///neue Regel als Dezimalzahl als Rückgabewert hat.
  self.changeRuleSet = function(newRule)
  {
    noLoop();
    const _newRule = newRule; ///hier checkBoxtoDEcimal einfügen
    self._automaton._ruleset = self.convertDecimalRule(_newRule);
    console.log("changed");
    console.log(self._automaton._ruleset);
  };
  
  /// das viewmodel wird nach dem ändern der Zelleigenschaften neu initialisiert, sodass alle viewModel-Eigenschaften
  /// mit der neuen Zellanzahl verfügbar sind.
  this.changeCellNumberAndProperties = function(difference)
  {	
    const _amountOfChange = Math.abs(difference);
    console.log(_amountOfChange);
    if(difference>0)
    {
      self._automaton.extendCellArray(_amountOfChange);
    }
    else
    {
      self._automaton.decreaseCellArray(_amountOfChange);
    }
    self.initialise();
    console.log(this._automaton._cells);
    console.log("Cell Width "+this._cellWidth);
    self.mediateIsRendered(false);
  };

  this.setInitialState = function(selected) 
  {
    const _selected = selected;
    switch (_selected)
    {
      case 1:
        self._automaton.initialiseMiddleCell();
        console.log("case01");
        break;
      case 2:
        self._automaton.initialiseRandomCells();
    }
    this.mediateIsRendered(false);
    redraw();
  }
  ////////////////////////////Programmabhängige Methoden für Custom View hier einfügen//////////
  this.calculateArcStart = function(index)
  {
    const _index = index;
    const xValue = this._automaton._cells[_index]._position* this._segmentArc;
    return xValue;
  };

  this.calculateArcStop = function(index)
  {
    const yValue = this.calculateArcStart(index) + this._segmentArc;
    return yValue;
  };

  this.calculateGenerationRadius = function(index)
  {
    const _index = index;
    const radius = (_index+1) * this._segmentHeight;
    return radius;
  };
}
