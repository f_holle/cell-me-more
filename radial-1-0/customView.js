function View(viewmodel, buffer) 
{
  let self = this;
  this._vm = viewmodel;
  this._buffer = buffer;
  this.zoom = 0;
  let x = 1;

  this._xOrigin = this._vm._bufferWidth/2;
  this._yOrigin = height/2;
  
  // Observer Pattern Methoden
  this.update = function (data)
  {
    this.render(automaton);
    image(this._buffer,0,0);
    this.zoom *= x/ (x+1);
    x++;
    console.log(this.zoom);
  }

  ///Kommunikation mit viewModel
  ///ruft die mediateIsRendered-Funktion des viewModels auf, die wiederum den übergebenen Wert 
  ///an die isRendered-Variable des Automaten weitergibt
  ///nur wenn isRendered = true wird die nächste generation berechnet.
  this.setIsRendered = function(value)
  {
    const givenValue = value;
    this._isRendered = givenValue;
    this._vm.mediateIsRendered(givenValue);
  }

  this.initialise = function(){
    this._buffer.background(255);
    this._buffer.noStroke();
    zoom = this._vm._bufferWidth /(this._vm._automaton._generationCount +1)* this._vm._bufferWidth;
  };

  this.render = function(automaton){
    /////füllen
    this._buffer.scale(this.zoom);
    {
      for(let i =0; i<automaton._cells.length; i++)
      {      
        this.renderSingleCellSegments(i);
      }
    }  
    this.setIsRendered(true);
  };

  this.clearAll = function()
  {
    this._buffer.clear();
    this._buffer.background(255);
    this._vm.reset();
  };
  ///////////////////
  this.renderSingleCellSegments = function(index)
  {
    const renderedCell = index;
    for (let i = this._vm._automaton._generationCount; i>=0; i--)
    {
      if (this._vm._automaton._cells[renderedCell]._history[i]==1)
      {
        this._buffer.fill(0);
      }
      else
      {
        this._buffer.fill(255);
      } 
        this._buffer.arc(this._xOrigin, this._yOrigin, this._vm.calculateGenerationRadius(i), 
        this._vm.calculateGenerationRadius(i), this._vm.calculateArcStart(renderedCell), this._vm.calculateArcStop(renderedCell));      
    }
  };
}
