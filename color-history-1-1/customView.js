/*
  In dieser Darstellungsform wird der Zustand jeder der 24 Zellen des AUtomaten als eine Stelle eines 24-bit RGB Farbwerts
  interpretiert. Dieser wird als Farbstreifen ausgegeben. Die Höhe der Zellen in der zuschaltbaren Standarddarstellung orientiert
  sich an der einstellbaren höhe der Farbstreifen.
 */ 

function View(viewmodel) 
{
  let self = this;
  this._vm = viewmodel;

  this.initialise = function(){
    background(255);
    noStroke();
  };

  this.render = function(automaton){
    if(this.isFullscreen)
    {
        scale(this._scalingX, this._scalingY);
    }
    
    {
      let r = this._vm.calculateR();
      let g = this._vm.calculateG();
      let b = this._vm.calculateB();
      noStroke();
      fill(r,g,b);
      rect(0, this._vm.getColorBarPositionY(), this._vm._bufferWidth, this._vm._cellWidth);
      this.setIsRendered(true);
     };
  };
}
