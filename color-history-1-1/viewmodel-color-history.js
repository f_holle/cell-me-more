function ViewModel(automato) 
{
  ////////////////////////////////////////NICHT ÄNDERN/////////////////////////////////////////

  let self = this; ///kann benutzt werden, wenn die Bezeichnung "this" missverständlich ist
  this._automaton = automato;
  this._isLooping = false;
  this.observers = [];

  this._cellWidth = 5;///Startwert

  this._bufferWidth = width;


  //Klassenmethoden
  this.initialise = function()
  {
    this.initialiseDefault();
  };

  ////////////////////////////Programmabhängige Methoden für Custom View hier einfügen//////////

  this.calculateR = function()
  {
    let rString = "";
    for (let i = 0; i<8; i++)
    {
      rString = rString+ this._automaton._cells[i]._state;
    }
    let rValue = parseInt(rString,2);
    return rValue;
  };
  
  this.calculateG = function()
  {
    let gString = "";
    for (let i = 8; i<16; i++)
    {
      gString = gString+ this._automaton._cells[i]._state;
    }
    let gValue = parseInt(gString,2);
    return gValue;
  };
  
  this.calculateB = function()
  {
    let bString = "";
    for (let i = 16; i<this._automaton._cells.length; i++)
    {
      bString = bString+ this._automaton._cells[i]._state;
    }
    let bValue = parseInt(bString,2);
    return bValue;
  };

  this.getColorBarPositionY = function()
  {
    const pos = this._cellWidth*this._automaton._generationCount;
    return pos;
  }

}
