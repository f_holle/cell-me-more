var configuration = 
{
    cellNumber: 24,
    cellNumberCustomizable: true,
    initialFrameRate: 10,
    frameRateMax: 60,
    isCyclicBydefault: false,

    defaultWidth: 800,
    defaultHeight: 600
}