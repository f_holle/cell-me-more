function View(viewmodel) 
{
  let self = this;
  this._vm = viewmodel;

  this.initialise = function()
  {
      background(255);
      noStroke();
  }

  this.render = function(automaton)
  {
    for (let i = 0; i<this._vm._numOfObjectsToUpdate; i++)
    {
        let parameter = this._vm.getActiveCellHistoryNumber(i);
        this._vm._cellHistoryStackArray[i].setStackSize(parameter);
        this._vm._cellHistoryStackArray[i].display();
    }  
    this.setIsRendered(true);
  }

}

////Container-Objekt für die information über einen Balken an einer Zellposition
function CellHistoryStack (positionX, initialHeight, myViewModel)
{
    this._viewModel = myViewModel;
    this._xScale = this._viewModel._cellWidth;
    this._position = positionX;
    this._stackSize = 0;

    ///Methoden

    this.setStackSize = function (value)
    {
        this._stackSize= value;
    }

    this.display = function()
    {
        let singleBlockPositionY = height - this._xScale;
        let i = 0;
        fill(0);
        console.log("hi");
        while (i< this._stackSize )
        {
            rect(this._position,singleBlockPositionY,this._xScale, this._xScale);
            singleBlockPositionY -= this._xScale;
            i++;
        }
    }

}
