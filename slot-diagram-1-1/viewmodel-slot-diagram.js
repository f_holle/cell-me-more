function ViewModel(automato) 
{
  ////////////////////////////////////////NICHT ÄNDERN/////////////////////////////////////////
  let self = this; ///kann benutzt werden, wenn die Bezeichnung "this" missverständlich ist
  this._automaton = automato;
  this._isLooping = false;
  this.observers = [];

  this._cellWidth = 0;
  this._edge = 0;
  this._backgroundcolor = color(51, 102, 204);
  this._cellHistoryStackArray = [];
  
  this._numOfObjectsToUpdate = 0;

  this._bufferWidth = width;


  //Klassenmethoden
  this.initialise = function()
  {
    this.initialiseDefault();
    this._numOfObjectsToUpdate = this._automaton._cellCount;
    this.initialiseCellStackArray();
  };

  ////////////////////////////Programmabhängige Methoden für Custom View hier einfügen//////////

  //// berechnet die Position der einzelnen Balkenelemente 
  this.getBarPosition = function(cellPosition)
  {
      let barPosition = cellPosition*this._cellWidth;
      return barPosition;
  }

  ////Bestimmt über das History-Array, wie oft eine einzelne Zelle in der Laufzeit des Automaten den Wert 1 hatte 
  this.getActiveCellHistoryNumber = function(index)
  {
      let numberOfActiveCells = 0;
      this._automaton._cells[index]._history.forEach(function(element){if (element == 1)numberOfActiveCells++});
      return numberOfActiveCells;
  }

  //erstellt ein CellHistoryStackArray mit der gleichen Anzahl an Elementen wie automaton._cells
  this.initialiseCellStackArray = function ()
  {
    this._cellHistoryStackArray = [];
    for (let i =0; i< this._numOfObjectsToUpdate; i++)
    {
        let positionX = this.getBarPosition(i);
        this._cellHistoryStackArray.push(new CellHistoryStack(positionX,0, this));
        this.resetCellStackArray();
    }
  };

  this.resetCellStackArray = function()
  {
     this._cellHistoryStackArray.forEach(function(element){element.setStackSize(0)});  
  };

}


