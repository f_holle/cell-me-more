function ViewModel(automaton) 
{
  ////////////////////////////////////////NICHT ÄNDERN/////////////////////////////////////////
  let self = this; ///kann benutzt werden, wenn die Bezeichnung "this" missverständlich ist
  this._automaton = automaton;
  this._isLooping = false;
  this.observers = [];

  this._cellWidth = 0;
  this._edge = 0;

  this._bufferWidth = width;
  this._cellStateDecimal = 0;
      


  //Klassenmethoden
  ///individuell für jedes VM:
  this.initialise = function()
  {
      this.initialiseDefault();
      this._cellStateDecimal = this.calculateDecimalNumber();
  };
    
  this.update = function()
  {
      this.updateDefault();
      this._cellStateDecimal = this.calculateDecimalNumber();
  }

  ////////////////////////////Programmabhängige Methoden für Custom View hier einfügen//////////
      this.calculateDecimalNumber = function ()
      {
        let cellStateString = "";
        this.currentStatesArray.forEach(element => {cellStateString = cellStateString + element});
        console.log(cellStateString);
        let cellStateDecimal = parseInt(cellStateString, 2);
        console.log (cellStateDecimal);
        return cellStateDecimal; 
      }
    }
