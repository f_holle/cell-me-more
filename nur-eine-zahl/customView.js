function View(viewmodel) 
{
  let self = this;
  this._vm = viewmodel;
  this._canvas = document.getElementById("defaultCanvas0");
  this.p5Context = this._canvas.getContext("2d");

  this.initialise = function()
  {
      background(255);
      noStroke();
      textSize(50);
  };

  this.setDisplayedDecimalNumber = function()
  {
    this._displayedDecimalNumber = ""+ this._vm._cellStateDecimal;
  }
    
  this.render = function(automaton){
    /////füllen
    if(this.isFullscreen)
    {
        scale(this._scalingX, this._scalingY);
    }
      
    background(255);
    this.setDisplayedDecimalNumber();
    textSize(this._textSize);
    fill(0);
    text(this._displayedDecimalNumber,20,height/2);
      
    this.setIsRendered(true);
  };
}



