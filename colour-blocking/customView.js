function View(viewmodel, buffer) 
{
  let self = this;
  this._vm = viewmodel;
  this._buffer = buffer;

  this.initialise = function(){
    this._buffer.background(200);
    this._buffer.noStroke();
  };

  this.render = function()
    {
      for (let i= 0; i<this._vm.colors.length; i++)
      {
        this._buffer.fill(this._vm.colors[i]);
        this._buffer.rect(this._vm.getBlockPositionX(i), this._vm.getCellPositionY(),this._vm._cellWidth,this._vm._cellWidth);
      }
    this._vm.resetColors;    
    this.setIsRendered(true);
    console.log("rendered");
  };

}
