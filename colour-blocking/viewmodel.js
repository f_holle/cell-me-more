function ViewModel(automato) 
{
  ////////////////////////////////////////NICHT ÄNDERN/////////////////////////////////////////
  let self = this; ///kann benutzt werden, wenn die Bezeichnung "this" missverständlich ist
  this._automaton = automato;
  this._isLooping = false;
  this.observers = [];

  this._cellWidth = 0;
  this._edge = 0;

  this._bufferGap = width*0.05;
  this._bufferWidth = (width/2) - this._bufferGap;
  this._additionalViewPosition = this._bufferWidth + this._bufferGap;

  /// variablen für die Darstellung der Blöcke
  this.colors = [];
  this.middle = this._bufferWidth/2;

  //Klassenmethoden
  this.initialise = function()
  {
    this.initialiseDefault();
  }

  this.update = function()
  {
    this.updateDefault();
    this.updateColors();
    this.notifyObservers();
  }

  ///gui-Input Callback Methoden
  
  ////// Schaltet die draw-Funtion in Abhängigkeit von IsLooping an oder ab
  this.runPauseButtonAction = function()
  {
    const boolean = !self._isLooping;
    self.setIsLooping(boolean);
    
    if (self._isLooping) 
    {
      loop();
    }
    else
    {
      noLoop();
    }
  };
  
  ///berechnet und rendert eine einzelne Generation
  this.renderOneGenerationButtonAction = function()
  {
    self.setIsLooping(false);
    noLoop();
    self._automaton.generateGeneration();  
  }

  this.changeArrayMode = function ()
  {
    console.log(self._automaton._isCyclic);
    if (self._automaton._isCyclic == false)
    {      
      console.log("hallo");
      self._automaton.setIsCyclic(true);
      self._automaton.closeRingGrid(); 
      console.log("is Cyclic");
    }
    else
    {
      self._automaton.disconnectRingGrid();
      self._automaton.setIsCyclic(false);
      console.log("isNotCyclic");
    }         
  } 

  ///wird von von den Regel-Inputs aufgerufen
  ///als Parameter wird die Change-Funktion des jeweiligen Inputs übergeben, die die
  ///neue Regel als Dezimalzahl als Rückgabewert hat.
  self.changeRuleSet = function(newRule)
  {
    noLoop();
    this.setIsLooping(false);
    const _newRule = newRule; ///hier checkBoxtoDEcimal einfügen
    self._automaton._ruleset = self.convertDecimalRule(_newRule);
    console.log("changed");
    console.log(self._automaton._ruleset);
  };
  
  /// das viewmodel wird nach dem ändern der Zelleigenschaften neu initialisiert, sodass alle viewModel-Eigenschaften
  /// mit der neuen Zellanzahl verfügbar sind.
  this.changeCellNumberAndProperties = function(difference)
  {	
    const _amountOfChange = Math.abs(difference);
    console.log(_amountOfChange);
    if(difference>0)
    {
      self._automaton.extendCellArray(_amountOfChange);
    }
    else
    {
      self._automaton.decreaseCellArray(_amountOfChange);
    }
    self.initialise();
    console.log(this._automaton._cells);
    console.log("Cell Width "+this._cellWidth);
    self.mediateIsRendered(false);
  };
  
  this.setInitialState = function(selected) 
  {
    const _selected = selected;
    switch (_selected)
    {
      case 1:
        self._automaton.initialiseMiddleCell();
        console.log("case01");
        break;
      case 2:
        self._automaton.initialiseRandomCells();
    }
    this.mediateIsRendered(false);
    redraw();
  }
  ////////////////////////////Programmabhängige Methoden für Custom View hier einfügen//////////
  
  //Rechnet die größe eines zusammenhängfenden Zellblocks in abhängigkeit von dessen Zustand in einen Farbwert um.
  //Aktive Zellen erhalten einen HSB-Farbwert, inaktive Zellen einen Grauwert.
  this.getColor = function(cellActive, cellNumber)
  {
    if (cellActive == true)
    {
      colorMode(HSB);
      const newColor = color('hsb(cellNumber, 100,0)');
      console.log(newColor);
      return newColor;
    }
    else 
    {
      colorMode(RGB);
      const newColor = color(cellNumber);
      console.log(newColor);
      return newColor;
    }
  };

  //Das Zell-Array wird Schritt für Schritt nach seinen Zuständen abgetatstet.
  //Bei jeder Iteration wird überprüft, ob der Zustand  der überprüften zellen gleich dem der
  //vorherigen ist. Die Anzahl zusammenhängernder Zellen wird dabei gespeichert.
  //Sobald sich der Zustand der überprüften zelle von dem vorherigen unterscheidet, wird die Größe
  //des vorangegangenen Blocks in einen Farbwert umgerechnet und dem colors-Array hinzugefügt.
  //nach der letzten überprüften Zelle wird unabhängig von ihrem Wert automatisch ein Block hinzugefügt.
  this.updateColors = function()
  {
    let cellsOfSameState = 0;
    for (let i=0; i<this._automaton._cellCount; i++)
    {
      let isCellActive = (this._automaton._cells[i]._state== 1)? true:false;
      if (i==0)
      {
        cellsOfSameState++;
      }
      else 
      {
        if(this._automaton._cells[i]._state == this._automaton._cells[i-1]._state)
        {
          cellsOfSameState ++;
          if (i == this._automaton._cellCount -1)
          {
            this.colors.push(this.getColor(isCellActive, cellsOfSameState));
          }
        }
        else
        {
          isCellActive != isCellActive;
          this.colors.push(this.getColor(isCellActive, cellsOfSameState));
          cellsOfSameState = 0;
        }
      }
    }
    console.log(this.colors);
  }

  this.getBlockPositionX = function(index)
  {
    const position = this.middle - (this.colors.length/2)*this._cellWidth + index*this._cellWidth;
    return position;
  };
  this.resetColors = function()
  {
    this.colors = [];
  } 

}
